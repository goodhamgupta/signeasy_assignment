# signeasy/server/main/views.py


from flask import Blueprint, jsonify, request
from signeasy.server.models import Member, Book

main_blueprint = Blueprint('main', __name__)

@main_blueprint.route("/api/members/", methods=["GET"])
def members():
    """
    Function to list all members.
    """
    queryset = Member.query.all()
    import pdb
    pdb.set_trace()
    return jsonify(members=[i.serialize for i in queryset]), 200

@main_blueprint.route("/api/members/<int:id>", methods=["GET"])
def get_member(id):
    """
    Function to retrieve a member

    :param id: ID of the member
    """
    member = Member.query.get(id)
    if member:
        return jsonify({"member": member}), 200
    else:
        return jsonify({"member": "Not found"}), 404

@main_blueprint.route("/api/members/", methods=["POST"])
def create_member():
    """
    Function to create a new member
    """
    payload = request.form
    member = Member(payload)
    if member:
        member.save()
        return jsonify({"message": "Created"}), 201
    else:
        return jsonify({"message": "Failed"}), 500

@main_blueprint.route("/api/members/<int:id>", methods=["PUT"])
def update_member(id):
    """
    Function to update an existing member.

    :param id: ID of the member to be updated.
    """
    payload = request.form
    member = Member.query.get(id)
    if member:
        member.update(payload)
        return jsonify({"message": "Updated"}), 204
    else:
        return jsonify({"message": "Failed! Member does not exist"}), 404

@main_blueprint.route("/api/members/<int:id>", methods=["DELETE"])
def delete_member(id):
    member = Member.query.find(id)
    if member:
        member.delete()
        return jsonify({"message": "Deleted"}), 200
    else:
        return jsonify({"message": "Member not found"}), 404

@main_blueprint.route("/api/members/books/<int:id>", methods=["PUT"])
def add_book(id):
    """
    Function to add a book to an existing member

    :param id: ID of the member to which the books should be assigned.
    """
    payload = request.form
    book_name = str(payload.get('name'))
    book = Book.query.filter_by(name=book_name).first()
    member = Member.query.get(id)
    import pdb
    pdb.set_trace()
    if book and member:
        (result, message) = member.add_book(book)
        if result:
            return jsonify({'message': message}), 204
        else:
            return jsonify({'message': message}), 500
    else:
        return jsonify({'message': "Not found"}), 404

@main_blueprint.route("/api/books/", methods=["GET"])
def books():
    """
    Function to return all available books.
    """
    queryset = Book.query.all()
    return jsonify(books=[i.serialize for i in queryset]), 200

@main_blueprint.route("/api/books/", methods=["POST"])
def create_book():
    """
    Function to create a new book.
    """
    payload = request.form
    book = Book(payload)
    if book:
        return jsonify({"message": "Created"}), 201
    else:
        return jsonify({"message": "Failed"}), 500

@main_blueprint.route("/api/books/<int:id>", methods=["GET"])
def get_book(id):
    """
    Function to fetch the existing book.

    :param id: ID of the book.
    """
    book = Book.query.get(id)
    if book:
        return jsonify({"book": book}), 200
    else:
        return jsonify({"book": "Not found"}), 404

@main_blueprint.route("/api/books/<int:id>", methods=["PUT"])
def update_book(id):
    """
    Function to update the attributes of a book.

    :param id: ID of the book to be updated.
    """
    payload = request.get_json()
    book = Book.query.get(id)
    if book:
        book.update(payload)
        return jsonify({"message": "Updated"}), 204
    else:
        return jsonify({"message": "Failed! Book does not exist"}), 404
