from datetime import datetime
from flask import current_app
from signeasy.server import db
from sqlalchemy.orm import validates

association_table = db.Table('association', db.Model.metadata,
    db.Column('member_id', db.Integer, db.ForeignKey('members.id')),
    db.Column('book_id', db.Integer, db.ForeignKey('books.id'))
)

class TimestampMixin(object):
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated = db.Column(db.DateTime, onupdate=datetime.utcnow, default=datetime.utcnow)

class Member(TimestampMixin, db.Model):
    __tablename__ = "members"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    registered = db.Column(db.Boolean, nullable=False, default=False)
    books = db.relationship("Book", secondary=association_table)

    @validates('email')
    def validate_email(self, key, email):
        assert '@' in email
        return email

    def __init__(self, params):
        self.email = str(params.get('email'))
        self.registered = True
        self.books = []
        self.save()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, payload):
        self.email = str(payload.get('email'))
        self.registered = payload.get('registered', self.registered)
        self.save()

    def add_book(self, book):
        if book.quantity > 0:
            self.books.append(book)
            book.update_quantity()
            self.save()
            return (True, "Success")
        else:
            return (False, "Book quantity cannot be negative!")

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def serialize(self):
       """
       Return object data in easily serializeable format
       """
       return {
           'id': self.id,
           'email': self.email,
           'registered': self.registered,
           'books'  : self.serialize_many2many
       }

    @property
    def serialize_many2many(self):
       """
       Calls many2many's serialize property.
       """
       return [ item.serialize for item in self.books]

class Book(TimestampMixin, db.Model):
    __tablename__ = "books"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), unique=False, nullable=False)
    author = db.Column(db.String(255), unique=False, nullable=False)
    quantity = db.Column(db.Integer, default=0)

    def __init__(self, params):
        self.name = str(params.get('name'))
        self.author = str(params.get('author'))
        self.quantity = int(params.get('quantity', 0))
        self.save()

    def update(self, payload):
        self.name = payload.get('name', self.name)
        self.author = param.get('author', self.author)
        self.quantity = params.get('quantity', self.quantity)
        self.save()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update_quantity(self):
        self.quantity = self.quantity - 1
        self.save()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def serialize(self):
       """
       Return object data in easily serializeable format
       """
       return {
           'id': self.id,
           'name': self.name,
           'author': self.author,
           'quantity': self.quantity
       }
